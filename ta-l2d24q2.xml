<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="https://bitbucket.org/lombardpress/lombardpress-schema/raw/master/LombardPressODD.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://bitbucket.org/lombardpress/lombardpress-schema/raw/master/LombardPressODD.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Librum II, Distinctio 24, Quaestio 2</title>
        <author>Thomas Aquinas</author>
        <editor/>
        <respStmt>
          <name>Jeffrey C. Witt</name>
          <resp>TEI Encoder</resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition n="0.0.0-dev">
          <title>Librum II, Distinctio 24, Quaestio 2</title>
          <date when="2016-03-17">March 17, 2016</date>
        </edition>
      </editionStmt>
      <publicationStmt>
        <publisher/>
        <pubPlace/>
        <availability status="free">
          <p>Public Domain</p>
        </availability>
        <date when="2016-03-17">March 17, 2016</date>
      </publicationStmt>
      <sourceDesc>
        <p>1856 editum</p>
      </sourceDesc>
    </fileDesc>
    <revisionDesc status="draft">
      <listChange>
        <change when="2015-09-06" who="#JW" status="draft" n="0.0.0-dev">
          <note type="validating-schema">lbp-0.0.1</note>
        </change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text xml:lang="la">
    <front>
      <div xml:id="includeList">
        <xi:include
          href="https://bitbucket.org/lombardpress/lombardpress-lists/raw/master/workscited.xml"
          xpointer="worksCited"/>
        <xi:include
          href="https://bitbucket.org/lombardpress/lombardpress-lists/raw/master/Prosopography.xml"
          xpointer="prosopography"/>
      </div>
      <div xml:id="starts-on"> </div>
    </front>
    <body>
      <div xml:id="ta-l2d24q2">
        <head>Librum II, Distinctio 24, Quaestio 2</head>

        <div>
          <head>Prooemium</head>
          <p>Deinde quaeritur de virtutibus libero arbitrio annexis; et quaeruntur quatuor: 1 de
            sensualitate, quid sit; 2 de superiori et inferiori parte rationis; 3 de synderesi; 4 de
            conscientia.</p>
        </div>
        <div type="articulus">
          <head>Articulus 1</head>

          <head type="questionTitle">Utrum notificatio sensualitatis posita in littera sit
            conveniens</head>

          <p>Ad primum sic proceditur. Videtur quod inconvenienter notificetur in littera
            sensualitas. Differt enim sensualitas a ratione, ut in littera dicitur. Sed inferior
            portio rationis est quaedam vis ex qua procedit appetitus rerum ad corpus pertinentium:
            quia temporalibus administrandis intendit, ut in littera dicitur. Ergo inconvenienter
            per haec sensualitas describitur.</p>
          <p>Praeterea, ut in littera dicitur, quidquid commune cum bestiis habemus, hoc ad
            sensualitatem pertinet. Sed vires sensitivae apprehensivae nobis pecoribusque communes
            sunt. Ergo ad sensualitatem pertinent. Sed ex apprehensivis non procedit appetitus et
            motus. Ergo inconvenienter per haec sensualitas describitur.</p>
          <p>Praeterea, secundum philosophum in 2 Elenc., eadem est ratio rei et unius rei. Sed
            sensualitas non est una vis animae: quia si colligit apprehensivas et appetitivas,
            constat quod plures sunt: similiter etiam si colligit appetitivas tantum, quia appetitus
            sensibilis, qui nobis et pecoribus communis est, in duo dividitur, scilicet in
            desiderium et in animum, ut in 3 de anima dicitur, sive irascibilem et concupiscibilem,
            quod idem est. Ergo videtur quod inconvenienter dicat sensualitatem esse vim animae
            quamdam.</p>
          <p>Praeterea, unius virtutis est unus actus. Sed ipsi sensualitati duos actus attribuit,
            idest appetitum et motum. Ergo inconvenienter eam describit sicut unam quamdam vim.</p>
          <p>Praeterea, si sensualitas est una vis, non potest esse nisi quod sit appetitus
            sensibilis. Sed appetitus non est ex appetitu. Ergo videtur quod inconvenienter dicat
            sensualitatem esse ex qua est appetitus rerum ad corpus pertinentium.</p>
          <p>Respondeo dicendum, quod differt sensualitas et sensibilitas; sensibilitas enim omnes
            vires sensitivae partis comprehendit, tam apprehensivas de foris, quam apprehensivas de
            intus, quam etiam appetitivas; sensualitas autem magis proprie illam tantum partem
            nominat per quam movetur animal in aliquod appetendum vel fugiendum. Sicut autem est in
            intelligibilibus, quod illud quod est apprehensum, non movet voluntatem nisi
            apprehendatur sub ratione boni vel mali, propter quod intellectus speculativus nihil
            dicit de imitando, vel fugiendo, ut in 3 de anima dicitur; ita etiam est in parte
            sensitiva, quod apprehensio sensibilis non causat motum aliquem, nisi apprehendatur sub
            ratione convenientis vel inconvenientis: et ideo dicitur in 2 de anima, quod ad ea quae
            sunt in imaginatione hoc modo nos habemus ac si essemus considerantes aliqua terribilia
            in picturis, quae passionem non excitarent vel timoris vel alicujus hujusmodi. Vis autem
            apprehendens hujusmodi rationes convenientis et non convenientis, videtur virtus
            aestimativa, per quam agnus fugit lupum et sequitur matrem; quae hoc modo se habet ad
            appetitum partis sensibilis, sicut se habet intellectus practicus ad appetitum
            voluntatis; unde, proprie loquendo, sensualitas incipit ex confinio aestimativae et
            appetitivae consequentis, ut hoc modo se habeat sensualitas ad partem sensitivam, sicut
            se habet voluntas et liberum arbitrium ad partem intellectivam. Hoc autem conveniens
            quod sensualitatem movet, aut ratio suae convenientiae, aut est apprehensa a sensu,
            sicut sunt delectabilia secundum singulos sensus, quae animalia persequuntur: aut est
            non apprehensa a sensu; sicut inimicitiam lupi neque videndo neque audiendo ovis
            percipit, sed aestimando tantum: et ideo motus sensualitatis in duo tendit: in ea
            scilicet quae secundum exteriores sensus delectabilia sunt; et hoc est quod dicitur,
            quod ex sensualitate est motus qui intenditur in corporis sensus: aut ad ea quae nociva
            vel convenientia corpori secundum solam aestimationem cognoscuntur; et sic ex
            sensualitate dicitur esse appetitus rerum ad corpus pertinentium.</p>
          <p>Ad primum ergo dicendum, quod ex ratione inferiori est etiam motus et appetitus eorum
            quae ad corpus pertinent; non tamen sicut ex proximo principio, sed sicut ex remoto,
            inquantum vires sensibiles per imperium movet, quae sunt aliqualiter obedientes rationi,
            ut in 1 Ethic. dicitur. Vel dicendum, quod ratio hujusmodi motum causat, non concernendo
            intentiones particulares et materiae concretas, sicut sensualitas; sed magis
            universales, et a materiae appendiciis separatas.</p>
          <p>Ad secundum dicendum, quod ad sensualitatem aliquid pertinet dupliciter: vel sicut
            existens de essentia ejus; et sic videtur tantum appetitivam partem continere: vel sicut
            praeambulum ad ipsum, sicut et ratio ad liberum arbitrium pertinet, ut dictum est: et
            hoc modo etiam vires apprehensivae sensitivae pertinent ad sensualitatem, licet secundum
            quemdam ordinem: quia aestimativa proprie se habet ad eam sicut ratio practica ad
            liberum arbitrium, quae etiam est movens; imaginatio autem simplex et vires praecedentes
            se habent magis remote, sicut ratio speculativa ad voluntatem.</p>
          <p>Ad tertium dicendum, quod sensualitas non nominat simpliciter unam potentiam, sed unam
            secundum genus, scilicet appetitivam sensitivam, quae in irascibilem et concupiscibilem
            dividitur. Sed tamen sciendum, quod ratione differunt sensualitas, et irascibilis et
            concupiscibilis. Cum enim, ut Dionysius dicit, natura inferior sui supremo, attingat
            infimum superioris naturae, natura sensitiva in aliquo sui quodammodo rationi
            conjungitur; unde et quaedam pars sensitiva, scilicet cognitiva, alio nomine ratio
            dicitur, propter confinium ejus ad rationem. Sic ergo dico, quod irascibilis et
            concupiscibilis nominant appetitum sensitivum, secundum quod completus est, et per
            diversa distinctus, et versus rationem tendens; unde et in homine irascibilis et
            concupiscibilis rationi obtemperant. Sensualitas autem nominat sensitivum appetitum,
            secundum quod est incompletus et indeterminatus, et magis depressus; et ideo dicitur,
            quod in ea non potest esse virtus, et quod est perpetuae corruptionis; et ex ipsa sua
            indeterminatione quamdam unitatem habet, ut quaedam vis dicatur.</p>
          <p>Ad quartum dicendum, quod si per motum intelligatur motus progressivus exterior, non
            inconvenienter motus et appetitus sensualitati attribuuntur, etiamsi sit una potentia,
            quia sunt actus non aeque primi, sed ordinem ad invicem habentes: appetitus enim
            interior motum exteriorem causat et sic unus mediante altero a sensualitate procedit. Si
            autem per motum intelligatur motus interior appetitus, tunc distinguuntur isti duo actus
            secundum diversa objecta, quae tamen potentiam non diversificant secundum genus: et hoc
            quonam modo sit, prius dictum est.</p>
          <p>Ad quintum dicendum, quod appetitus est nomen potentiae et nomen actus: unde non est
            inconveniens quod ex appetitu potentiae procedat appetitus actus.</p>
        </div>
        <div type="articulus">
          <head>Articulus 2</head>

          <head type="questionTitle">Utrum ratio superior et inferior sit una potentia</head>

          <p>Ad secundum sic proceditur. Videtur quod ratio superior et inferior non sit una
            potentia, sed diversae. In partibus enim animae non accipitur superius et inferius
            animae secundum situm sed secundum dignitatem. Sed inter partes animae intellectivae est
            invenire unam partem alia digniorem: quia intellectus agens est nobilior possibili ut in
            3 de anima dicitur, quae diversae potentiae sunt, ut supra dictum est. Ergo videtur quod
            id quod est superius in ratione et id quod est inferius sint diversae potentiae.</p>
          <p>Praeterea, aeternum et necessarium idem esse videtur, ut contingens et temporale. Sed
            philosophus dicit, quod scientificum animae circa necessaria est, ratiocinativum autem
            circa contingentia operata a nobis. Ergo videtur quod scientificum sit illud idem quod
            ratio superior, quae aeterna conspicit; et ratiocinativum idem quod ratio inferior, quae
            temporalibus inhaeret, ut in littera dicitur. Sed ratiocinativum et scientificum, ut
            ibidem dicitur, sunt diversae potentiae. Ergo videtur quod etiam ratio superior et
            inferior.</p>
          <p>Praeterea, in superiori parte animae est imago deitatis, ut in 1 libro dictum est. Sed
            superior pars animae est superior pars rationis. Ergo in superiori ratione est imago.
            Sed imago colligit tres potentias, memoriam intelligentiam et voluntatem. Ergo ratio
            superior et inferior non dicunt unam quamdam potentiam, sed plures.</p>
          <p>Praeterea, regulans et regulatum et imperans et imperatum non possunt esse idem, sicut
            nec agens et patiens. Sed ratio superior se habet ad inferiorem sicut regulans ad
            regulatum. Ergo non possunt esse una potentia.</p>
          <p>Praeterea, potentiae distinguuntur per actus. Sed officium actum nominat, ut Tullius
            dicit. Ergo videtur, cum ratio superior et inferior per diversa officia geminentur, quod
            sint diversae potentiae.</p>
          <p>Sed contra, diversitas potentiarum constituit diversitatem rei. Sed in littera dicitur,
            quod cum de superiori et inferiori ratione loquimur, de una quadam re dicimus. Ergo
            videtur quod non sint diversae potentiae.</p>
          <p>Praeterea, potentia per diversos habitus non diversificatur. Sed ratio superior
            dicitur, prout dono sapientiae perficitur: inferior, prout dono scientiae, ut in littera
            dicitur. Ergo videtur quod superior et inferior ratio non sint diversae potentiae.</p>
          <p>Respondeo dicendum, quod ratio hic accipitur quae hoc modo se habet ad voluntatem et
            liberum arbitrium, sicut se habet apprehensio sensitiva ad sensualitatem: sicut enim
            dictum est, nullus appetitus movetur in suum objectum nisi fiat apprehensio alicujus sub
            ratione boni vel mali, convenientis vel nocivi. Hanc autem rationem convenientis et boni
            aliter homo percipit, aliter brutum: brutum enim non conferendo, sed quodam naturali
            instinctu sibi conveniens vel nocivum, cognoscit; homo autem per investigationem quamdam
            et collationem hujusmodi rationes considerat; et ideo vis illa per quam in hujusmodi
            rationum cognitionem venit consequenter ratio dicitur, quae investigativa est, et
            deductiva unius in alterum. Quia vero tota ratio potentiarum ex objectis sumitur, quorum
            speciebus informantur, inde est quod oportet in ratione quemdam gradum constituere
            secundum ordinem eorum quibus intendit. In rebus autem quas ratio considerat, talis
            invenitur distinctio et ordo, ut quaedam aeterna et necessaria, a temporalibus discreta,
            eis proponantur; unde et ratio ex hoc quemdam gradum consequitur quod his vel aliis
            intendit. Sed quia ita est in ordine rerum quod superius est directivum inferioris et
            causa, inde est quod per aeterna in his quae temporalia sunt diriguntur, sicut id quod
            uno modo se habet, est mensura ejus quod multiforme est, ut ex 10 Metaphys. accipitur.
            Et secundum hoc patet quod ratio aeternis dupliciter inhaerere potest: vel considerando
            ipsa in se, vel considerando ipsa secundum quod sunt regula temporalium per nos
            disponendorum et agendorum: et prima consideratio non exit limites speculativae
            rationis; secunda autem ad genus practicae rationis pertinet. Unde patet quod ratio
            superior partim est speculativa et partim practica, et ideo in littera dicitur, quod
            supernis conspiciendis, inquantum est speculativa, et inquantum est practica, supernis
            consulendis intendit. Unde ex hoc patet quod ratio superior, prout contra inferiorem
            dividitur, non distat ab ea sicut speculativum et practicum, quasi ad diversa objecta
            respiciant, de quibus fiat ratiocinatio; sed magis distinguuntur secundum media, unde
            ratiocinatio sumitur; ratio enim inferior consiliatur ad electionem tendens ex
            rationibus rerum temporalium, ut quod aliquid est superfluum vel diminutum, utile vel
            honestum, et sic de aliis conditionibus quas moralis philosophus pertractat; superior
            vero consilium sumit ex rationibus aeternis et divinis, ut quia est contra praeceptum
            Dei, vel ejus offensionem parit, vel aliquid hujusmodi. Diversitas autem mediorum, ex
            quibus ad idem genus conclusionis proceditur, non potest facere diversam potentiam, sed
            quandoque diversum habitum; et ideo ratio superior et inferior non distinguuntur sicut
            diversae potentiae, sed magis secundum habitum, vel quem jam actu habet, vel ad quem
            naturaliter ordinatur: ratio enim superior perficitur sapientia, sed inferior
            scientia.</p>
          <p>Ad primum ergo dicendum, quod ratio superior et inferior non differunt sicut agens et
            possibile, quod sic patet. Quia ut in littera dicitur, ratio superior et inferior habent
            actus respectu diversorum. Agens autem et possibile semper concurrunt ad idem objectum
            vel medium: quia impossibile est nos in intellectivam operationem progredi sine
            operatione possibilis et agentis: oportet enim ut species phantasmatum, quae sunt
            objecta intellectus nostri, efficiantur in actu intelligibiles, quod ad agentem
            pertinet; et intellectui conjungantur in eo receptae, quod pertinet ad possibilem. Unde
            ex diversitate possibilis et agentis non sequitur diversitas superioris et inferioris
            rationis; sed ex diversitate medii vel objecti.</p>
          <p>Ad secundum dicendum, quod scientificum et ratiocinativum non omnino distinguuntur
            sicut ratio superior et inferior: quia scientificum nullo modo ad praxim pertinet, sicut
            pertinet ratio superior, ut dictum est, inquantum scilicet aeterna consulit, et praeter
            hoc scientificum ad quaedam se extendit quorum non est ratio superior, prout hic
            accipitur, scilicet ad res creatas necessarias: quia philosophus scientificum animae non
            tantum sapientia quae divinorum est proprie, sed scientia et intellectu, quae creatorum
            sunt, in 6 Ethic. perfici docet. Cognitio autem rerum temporalium sive quantum ad ea
            quae ad nos agenda pertinent sive quantum ad ea quae in his necessariis
            demonstrationibus considerantur, ad rationem inferiorem pertinet, quae scientia
            perficitur, quam Augustinus extendit tam ad speculativam quam ad practicam
            considerationem rerum temporalium; unde distinctio superioris et inferioris rationis non
            est idem cum distinctione scientifici et ratiocinativi, quamvis scientificum secundum
            aliquid sui, cum ratione superiori conveniat, et ratio inferior cum ratiocinativo.</p>
          <p>Ad tertium dicendum, quod ratio superior non est omnino idem cum illa parte mentis in
            qua consistit imago, sed includit eam et excedit; quod sic patet. Imago enim potissime
            distinguitur secundum hoc quod mens tendit in objectum quod Deus est, ut in 1 libro
            dictum est; unde potentiae imaginis prout ad imaginem pertinent respiciunt aeterna
            solummodo ut objectum, ratio autem superior considerat ea dupliciter, scilicet ut
            objectum, inquantum conspicit ea, et ut medium inquantum ipsa consulit: nihilominus
            tamen etsi imago plures potentias essentialiter colligat, non oportet quod ratio
            superior in pluribus potentiis consistat: quia imago comprehendit et cognitivam et
            affectivam, sed ratio comprehendit imaginem secundum cognitivam tantum et excedit, ut
            dictum est: et ideo ratio superior et mens in qua est imago se habent ut excedentia et
            excessa; superior enim ratio est speculativa et practica, sed mens secundum quod in ea
            est imago tantum, ad speculativam pertinet, quia objectum imaginis non est aliquid
            operabile a nobis: et sic ratio superior excedit mentem, et exceditur a mente, inquantum
            mens comprehendit affectionem et cognitionem; cum ratio cognitionem tantum importet.</p>
          <p>Ad quartum dicendum, quod diversitas regulantis et regulati non demonstrat diversam
            potentiam, sed diversum habitum: unus enim habitus est regulativus alterius, sicut patet
            in scientiis speculativis quod omnes scientias sapientia, scilicet metaphysica, dirigit.
            Ita etiam ratio superior inferiorem dirigere dicitur.</p>
          <p>Ad quintum dicendum, quod non quaelibet diversitas actus ostendit diversitatem
            potentiae; sed quandoque etiam ostendit tantum diversitatem habitus, sicut geometrizare,
            et syllogizare; quandoque autem neutrum. Ista autem sic patent: quia enim substantia
            uniuscujusque potentiae est, secundum quod est nata operari circa proprium objectum, ut
            de sensu dicit philosophus in 2 de anima, ideo actiones quae differunt secundum diversa
            objecta, ostendunt diversitatem potentiarum: ut tamen accipiatur differentia objectorum
            secundum id quod ad propriam rationem objecti pertinet: homo enim et lapis differunt
            genere, sed conveniunt secundum quod sunt objectum visus in colore: et ideo visio
            hominis et lapidis pertinent ad unam potentiam; sed sentire sonum et colores pertinent
            ad diversas potentias: quia sonus et color, secundum proprias rationes, quibus ad
            invicem distinguuntur, sunt propria objecta sensus. Quandoque autem diversitas actuum
            causatur ex diversitate medii, vel principii, ex quo pervenitur ad idem genus objecti:
            et talis diversitas actuum ostendit diversitatem habituum: diversae enim scientiae ex
            diversis principiis procedunt, etiam si easdem conditiones demonstrent; sicut astrologus
            et naturalis diversis mediis rotunditatem terrae ostendit, ut dicitur in 2 Phys.
            Similiter etiam virtutes morales distinguuntur ex diversis finibus, qui sunt in
            operativis sicut principia in speculativis. Quandoque vero diversitas actuum causatur ex
            eo quod est accidens actionis; vel ex parte agentis, secundum quod est potentius vel
            infirmius in agendo, sicut hebetudo vel subtilitas ingenii, quae differunt secundum
            velocitatem et tarditatem addiscendi; vel ex parte medii, ut credere et opinari, quae
            differunt secundum efficaciam et debilitatem medii; vel ex parte objecti, sicut videre
            hominem et lapidem; accidit enim colorato esse hominem aut lapidem: et talis diversitas
            actionum neque potentiam diversam neque diversum habitum requirit: quia illud quod est
            per accidens, non causat differentiam in specie. Officia autem rationis superioris et
            inferioris non differunt penes diversam rationem objecti, cum utrumque operabilia
            consideret, sed penes diversam rationem medii: quia ratio inferior procedit ex
            rationibus temporalibus; sed ratio superior ex rationibus aeternis; et ita etiam haec
            diversa officia non oportet quod diversas potentias demonstrent, sed diversos habitus,
            ut dictum est.</p>
        </div>
        <div type="articulus">
          <head>Articulus 3</head>

          <head type="questionTitle">Utrum synderesis sit habitus, vel potentia</head>

          <p>Ad tertium sic proceditur. Videtur quod synderesis sit potentia, et non habitus. Ea
            enim quae veniunt in eamdem divisionem, videntur esse unius rationis. Sed synderesis
            dividitur contra alias animae potentias, scilicet contra rationalem, et concupiscibilem
            et irascibilem, ut patet ex Glossa Hieronymi Ezech. 1. Ergo videtur quod sit
            potentia.</p>
          <p>Praeterea, Hieronymus dicit Malach. 2, super illud: <quote>custodite spiritum
              vestrum</quote> etc.: <quote>spiritus dicitur, non pars animalis, quae non percipit ea
              quae sunt Dei, sed rationalis</quote>. Hanc autem vocat synderesim. Sed rationalis
            pars potentiam nominat. Ergo videtur quod sit potentia.</p>
          <p>Praeterea, habitus non inscribitur nisi potentiae. Sed Augustinus dicit, quod
            universalia juris praecepta scripta sunt in naturali judicatorio, quod est synderesis.
            Ergo cum universalium juris praeceptorum sit aliquis habitus, videtur quod synderesis,
            cui inscribuntur, sit potentia quaedam.</p>
          <p>Praeterea, ex identitate actuum colligitur identitas potentiarum. Sed, ut ex inducta
            auctoritate patet, ad synderesim pertinet judicium. Cum ergo liberum arbitrium a
            judicando nominetur, videtur quod synderesis sit idem quod liberum arbitrium. Sed
            liberum arbitrium est potentia. Ergo et synderesis.</p>
          <p>Praeterea, habitus amittitur per oblivionem, vel alio modo. Sed synderesis semper
            manet, quae etiam post mortem peccato remurmurat, cujus murmur vermis dicitur. Ergo
            synderesis nominat potentiam, et non habitum.</p>
          <p>Sed contra, potentia rationalis se habet ad opposita. Sed synderesis se habet
            determinate ad unum, quia nunquam errat. Ergo videtur quod non sit potentia, sed
            habitus.</p>
          <p>Praeterea, opposita in idem genus reducuntur. Sed synderesi opponitur fomes: sicut enim
            fomes semper ad malum instigat, ita et synderesis semper in bonum tendit. Cum igitur
            fomes sit habitus quidam, ut in littera dicitur, videtur etiam quod synderesis habitum
            nominet.</p>
          <p>Respondeo dicendum, quod sicut est de motu rerum naturalium, quod omnis motus ab
            immobili movente procedit, ut dicit Augustinus 8 super Genes. et philosophus probat in 7
            Phys., et 8, et omne dissimiliter se habens ab uno eodemque modo se habente; ita etiam
            oportet quod sit in processu rationis; cum enim ratio varietatem quamdam habeat, et
            quodammodo mobilis sit, secundum quod principia in conclusiones deducit, et in
            conferendo frequenter decipiatur; oportet quod omnis ratio ab aliqua cognitione
            procedat, quae uniformitatem et quietem quamdam habeat; quod non fit per discursum
            investigationis, sed subito intellectui offertur: sicut enim ratio in speculativis
            deducitur ab aliquibus principiis per se notis, quorum habitus intellectus dicitur; ita
            etiam oportet quod ratio practica ab aliquibus principiis per se notis deducatur, ut
            quod est malum non esse faciendum, praeceptis Dei obediendum fore, et sic de aliis: et
            horum quidem habitus est synderesis. Unde dico, quod synderesis a ratione practica
            distinguitur non quidem per substantiam potentiae, sed per habitum, qui est quodammodo
            innatus menti nostrae ex ipso lumine intellectus agentis, sicut et habitus principiorum
            speculativorum, ut, omne totum est majus sua parte, et hujusmodi; licet ad
            determinationem cognitionis eorum sensu et memoria indigeamus, ut in 2 Post. dicitur. Et
            ideo statim cognitis terminis, cognoscuntur, ut in 1 Poster. dicitur. Et ideo dico, quod
            synderesis vel habitum tantum nominat, vel potentiam saltem subjectam habitui sic nobis
            innato.</p>
          <p>Ad primum ergo dicendum, quod synderesis dividitur contra alias potentias, non quasi
            diversa per substantiam potentiae sed per habitum quemdam; sicut si intellectus
            principiorum contra speculativam rationem divideretur.</p>
          <p>Ad secundum dicendum, quod rationalis pars non simpliciter vocatur synderesis, sed
            secundum quod talem habitum concernit.</p>
          <p>Ad tertium dicendum, quod universalia juris non inscribuntur synderesi, quasi habitus
            potentiae, sed magis quasi collecta in habitu inscribuntur ipsi habitui; sicut principia
            geometricalia geometriae inscribuntur.</p>
          <p>Ad quartum dicendum, quod judicium non eodem modo libero arbitrio et synderesi
            convenit: quia ad synderesim pertinet universale judicium, secundum universalia juris
            principia: semper enim de conclusionibus per principia judicatur; unde et scientia
            resolutiva judicandi ars dicitur: sed ad liberum arbitrium pertinet judicium particulare
            de hoc operabili, quod est judicium electionis. Unde synderesis non est idem quod
            liberum arbitrium.</p>
          <p>Ad quintum dicendum, quod habitus naturalis nunquam amittitur, sicut patet de habitu
            principiorum speculativorum, quem semper homo retinet; et simile est etiam de
            synderesi.</p>
        </div>
        <div type="articulus">
          <head>Articulus 4</head>

          <head type="questionTitle">Utrum conscientia sit actus</head>

          <p>Ad quartum sic proceditur. Videtur quod conscientia non sit actus. Origenes enim dicit,
            quod conscientia est spiritus corrector et paedagogus animae, sibi sociatus, quo
            separatur a malis et adhaeret bonis. Sed spiritus vel nominat potentiam, vel etiam ipsam
            essentiam animae. Ergo videtur quod conscientia non sit actus.</p>
          <p>Praeterea, conscientia, secundum quod a quibusdam describitur, est dictamen rationis,
            quo judicat, et ligat ad faciendum et non faciendum. Sed judicium de hoc faciendo vel
            non faciendo, ut dictum est, pertinet ad liberum arbitrium. Ergo conscientia est liberum
            arbitrium. Sed liberum arbitrium non est actus. Ergo nec conscientia.</p>
          <p>Praeterea, in Glossa Ezech. 1, dicit Hieronymus postquam de synderesi locutus est:
              <quote>hanc autem conscientiam interdum praecipitari videmus</quote>. Ergo videtur
            quod conscientia sit idem quod synderesis. Sed synderesis non nominat actum, sed
            potentiam vel habitum. Ergo videtur quod etiam conscientia.</p>
          <p>Praeterea, conscientia est scientia quaedam. Sed scientia nominat habitum. Ergo et
            conscientia.</p>
          <p>Praeterea, Damascenus dicit, quod conscientia est lex intellectus nostri. Sed lex
            intellectus est ipsa lex naturalis, quae est habitus principiorum juris. Ergo videtur
            quod conscientia sit habitus, et non actus.</p>
          <p>Praeterea, omnis actus vel generat habitum, vel saltem ex aliquo habitu est productus.
            Si igitur conscientia est actus quidam, oportet quod multiplicatis actibus generetur
            habitus, qui conscientia dicatur; vel quod saltem tales actus ex habitu aliquo
            procedant, quod in idem redit.</p>
          <p>Sed contra, conscientia peccatum aggravare dicitur. Sed aggravatio peccati esse non
            potest nisi per hoc quod contradicitur actuali rationis considerationi. Ergo videtur
            quod conscientia actualem rationis considerationem nominet.</p>
          <p>Praeterea, ut dictum est, conscientia a quibusdam dictamen rationis dicitur. Sed
            dictamen actum quemdam nominat, secundum quod ratio aliquid faciendum dijudicat. Ergo
            videtur quod conscientia actum nominet.</p>
          <p>Respondeo dicendum, quod conscientia multis modis accipitur. Quandoque enim dicitur
            conscientia ipsa res conscita; et sic sumitur 1 Tim. 1, 5: <quote>caritas procedit de
              conscientia bona</quote>. Glossa: <quote>idest spes; quia ex meritis quae conscientia
              tenet, motus spei insurgit</quote>. Quandoque vero dicitur habitus, quo quis
            disponitur ad consciendum; et secundum hoc ipsa lex naturalis et habitus rationis
            consuevit dici conscientia. Quidam etiam dicunt, quod conscientia quandoque potentiam
            nominat; sed hoc nimis extraneum est, et improprie dictum: quod patet, si diligenter
            omnes potentiae animae inspiciantur. Nullo autem horum modorum conscientia sumitur,
            secundum quod in usum loquentium venit, prout dicitur ligare vel aggravare peccatum:
            nullus enim ligatur ad aliquid faciendum nisi per hoc quod considerat hoc esse agendum;
            unde quamdam actualem considerationem rationis, per conscientiam, communiter loquentes
            intelligere videntur: sed quae sit illa actualis rationis consideratio, videndum est.
            Sciendum est igitur, quod, sicut in 6 Ethic. philosophus dicit, ratio in eligendis et
            fugiendis, quibusdam syllogismis utitur. In syllogismo autem est triplex consideratio,
            secundum tres propositiones, ex quarum duabus tertia concluditur. Ita etiam contingit in
            proposito, dum ratio in operandis ex universalibus principiis circa particularia
            judicium assumit. Et quia universalia principia juris ad synderesim pertinent, rationes
            autem magis appropriatae ad opus, pertinent ad habitus, quibus ratio superior et
            inferior distinguuntur; synderesis in hoc syllogismo quasi majorem ministrat, cujus
            consideratio est actus synderesis; sed minorem ministrat ratio superior vel inferior, et
            ejus consideratio est ipsius actus; sed consideratio conclusionis elicitae, est
            consideratio conscientiae. Verbi gratia, synderesis hanc proponit: omne malum est
            vitandum: ratio superior hanc assumit: adulterium est malum, quia lege Dei prohibitum:
            sive ratio inferior assumeret illam, quia ei est malum, quia injustum, sive inhonestum:
            conclusio autem, quae est, adulterium hoc esse vitandum, ad conscientiam pertinet, et
            indifferenter, sive sit de praesenti vel de praeterito vel futuro: quia conscientia et
            factis remurmurat, et faciendis contradicit: et inde dicitur conscientia, quasi cum alio
            scientia, quia scientia universalis ad actum particularem applicatur: vel etiam quia per
            eam aliquis sibi conscius est eorum quae fecit, vel facere intendit: et propter hoc
            etiam dicitur sententia, vel dictamen rationis: et propter hoc etiam contingit
            conscientiam errare, non propter synderesis errorem, sed propter errorem rationis; sicut
            patet in haeretico, cui dictat conscientia quod prius permittat se comburi quam juret:
            quia ratio superior perversa est in hoc quod credit, juramentum simpliciter esse
            prohibitum. Et secundum hunc modum patet, qualiter differant synderesis, lex naturalis,
            et conscientia: quia lex naturalis nominat ipsa universalia principia juris, synderesis
            vero nominat habitum eorum, seu potentiam cum habitu; conscientia vero nominat
            applicationem quamdam legis naturalis ad aliquid faciendum per modum conclusionis
            cujusdam.</p>
          <p>Ad primum ergo dicendum, quod spiritus quandoque sumitur pro spirituali quadam
            operatione, et pro spirituali dono; et hoc modo sumitur hic spiritus, et non pro natura
            animae.</p>
          <p>Ad secundum dicendum, quod judicium ad liberum arbitrium pertinet, ad conscientiam, et
            synderesim; sed diversimode; quia ad liberum arbitrium pertinet judicium quasi
            participative, quia per se voluntatis non est judicare; unde ipsum judicium electionis
            liberi arbitrii est: sed judicium per se vel est in universali, et sic pertinet ad
            synderesim; vel est in particulari, tamen infra limites cognitionis persistens, et
            pertinet ad conscientiam; unde tam conscientia quam electio, conclusio quaedam est
            particularis vel agendi vel fugiendi; sed conscientia conclusio cognitiva tantum,
            electio conclusio affectiva: quia tales sunt conclusiones in operativis, ut in 6 Ethic.
            dicitur.</p>
          <p>Ad tertium dicendum, quod tota virtus conclusionis ex primis principiis trahitur; et
            inde est quod conscientia et synderesis frequenter pro eodem accipiuntur, et judicium
            utrique attribuitur, et praecipue judicium universale, quod per delectationem peccati
            non corrumpitur, sed magis judicium in particularibus: et ideo non dixit synderesim
            praecipitari, sed conscientiam.</p>
          <p>Ad quartum dicendum, quod conscientia et actum et habitum nominare potest: est enim
            scire in habitu et scire in actu; unde et conscientia diversimode accipi potest, ut
            dictum est.</p>
          <p>Ad quintum dicendum, quod conscientia, secundum quod accipitur pro habitu, potest dici
            lex naturalis, quia ex habitu illo praecipue actus conscientiae elicitur.</p>
          <p>Ad sextum dicendum, quod habitus ille ex quo nascitur actus conscientiae, non est
            habitus separatus ab habitu rationis et synderesi: quia non alius habitus est
            principiorum et conclusionum quae eliciuntur ab eis, et praecipue earum quae sunt circa
            singularia, quorum non est habitus scientiae, nisi secundum quod continentur in
            principiis universalibus.</p>
        </div>
        <div>
          <head>Expositio textus</head>
          <p><quote>Sed non perficere bonum</quote>. Intelligendum est de bono meritorio.
              <quote>Quia nihil in eo erat quod ad malum impelleret</quote>. Videtur haec causa esse
            insufficiens: quia in Christo etiam nihil fuit ad malum impellens, et tamen resistendo
            malo meruit. Et dicendum, secundum quosdam, quod hoc intelligitur non de quolibet
            merito, sed de merito satisfactionis, ad quam poena requiritur. Sed hoc non videtur esse
            ad propositum, quia Adam merito satisfactionis non indigebat: et ideo aliter potest
            dici, quod ratio meriti ex duobus potest sumi; vel ex habitu informante; et sic omnis
            actus, vel facilis vel difficilis, gratia informatus meritorius est: vel ex conditione
            actus, praecipue in quo est difficultas; et hanc rationem merendi in resistendo peccato
            non habuit.</p>
        </div>

      </div>
    </body>
  </text>
</TEI>
